## Установка

``` bash
# установите зависимости
npm install
```

### Запуск сборки с browser-sync

``` bash
# сервер с browser-sync поднимется по адресу http://localhost:3000
npm run dev
```

### Запуск сборки для сервера

``` bash
# сборка стилей и прочего без browser-sync
npm run release
```
После успешной сборки в папке сайта создается директория build
