class Products {
    constructor(container) {
        this.container = container;

        this.URL = 'https://reqres.in/api/products/';
        this.STORAGE_KEY = 'productsInCart';

        this.setListeners();
        this.checkProductsInBasket();
    }

    setListeners() {
        [...this.container.querySelectorAll('.js-product-buy')].forEach(item => {
            item.addEventListener('click', event => {
                event.preventDefault();

                if (event.target.classList.contains('btn--in-basket')) {
                    return;
                }

                this.addProduct(event.target);
            });
        });
    }

    addProduct(button) {
        const { productId } = button.dataset;

        button.classList.add('btn--loading');

        fetch(this.URL + productId)
            .then(() => {
                button.classList.remove('btn--loading');
                this.setButtonInBasket(button);

                this.addProductToStorage(productId);
            })
            .catch(error => {
                console.error(error);
            });
    }

    // eslint-disable-next-line class-methods-use-this
    setButtonInBasket(button) {
        button.classList.add('btn--in-basket');
        // eslint-disable-next-line no-param-reassign
        button.textContent = 'В корзине';
    }

    addProductToStorage(id) {
        let products = localStorage.getItem(this.STORAGE_KEY);

        if (!products) {
            products = [];
        } else {
            products = JSON.parse(products);
        }

        products.push(id);
        localStorage.setItem(this.STORAGE_KEY, JSON.stringify(products));
    }

    checkProductsInBasket() {
        let products = localStorage.getItem(this.STORAGE_KEY);

        if (!products) {
            return;
        }

        products = JSON.parse(products);

        products.forEach(id => {
            const button = document.querySelector(`[data-product-id="${id}"]`);
            this.setButtonInBasket(button);
        });
    }
}

document.addEventListener('DOMContentLoaded', () => {
    const products = document.querySelectorAll('.js-products');
    [...products].forEach(item => new Products(item));
});
