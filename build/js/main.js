"use strict";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Products = /*#__PURE__*/function () {
  function Products(container) {
    _classCallCheck(this, Products);

    this.container = container;
    this.URL = 'https://reqres.in/api/products/';
    this.STORAGE_KEY = 'productsInCart';
    this.setListeners();
    this.checkProductsInBasket();
  }

  _createClass(Products, [{
    key: "setListeners",
    value: function setListeners() {
      var _this = this;

      _toConsumableArray(this.container.querySelectorAll('.js-product-buy')).forEach(function (item) {
        item.addEventListener('click', function (event) {
          event.preventDefault();

          if (event.target.classList.contains('btn--in-basket')) {
            return;
          }

          _this.addProduct(event.target);
        });
      });
    }
  }, {
    key: "addProduct",
    value: function addProduct(button) {
      var _this2 = this;

      var productId = button.dataset.productId;
      button.classList.add('btn--loading');
      fetch(this.URL + productId).then(function () {
        button.classList.remove('btn--loading');

        _this2.setButtonInBasket(button);

        _this2.addProductToStorage(productId);
      })["catch"](function (error) {
        console.error(error);
      });
    } // eslint-disable-next-line class-methods-use-this

  }, {
    key: "setButtonInBasket",
    value: function setButtonInBasket(button) {
      button.classList.add('btn--in-basket'); // eslint-disable-next-line no-param-reassign

      button.textContent = 'В корзине';
    }
  }, {
    key: "addProductToStorage",
    value: function addProductToStorage(id) {
      var products = localStorage.getItem(this.STORAGE_KEY);

      if (!products) {
        products = [];
      } else {
        products = JSON.parse(products);
      }

      products.push(id);
      localStorage.setItem(this.STORAGE_KEY, JSON.stringify(products));
    }
  }, {
    key: "checkProductsInBasket",
    value: function checkProductsInBasket() {
      var _this3 = this;

      var products = localStorage.getItem(this.STORAGE_KEY);

      if (!products) {
        return;
      }

      products = JSON.parse(products);
      products.forEach(function (id) {
        var button = document.querySelector("[data-product-id=\"".concat(id, "\"]"));

        _this3.setButtonInBasket(button);
      });
    }
  }]);

  return Products;
}();

document.addEventListener('DOMContentLoaded', function () {
  var products = document.querySelectorAll('.js-products');

  _toConsumableArray(products).forEach(function (item) {
    return new Products(item);
  });
});